(function($) {
  Drupal.behaviors.brom_mod_hosting_platform = {
    attach: function(context, settings) {

      Drupal.toggleFieldset($('#edit-git:not(.collapsed)'));
      Drupal.toggleFieldset($('#edit-frommakefile.collapsed'));

    }
  }
})(jQuery);
