(function($) {
  Drupal.behaviors.brom_mod = {
    attach: function(context, settings) {

      $('#brom-toggle-all-fieldsets').show();
      $('#brom-toggle-all-fieldsets').click(function () {
        var elements = $(this).text() == "Collapse all" ? $("fieldset").not(".collapsed") : $("fieldset.collapsed");
        $(elements).each(function () {
          Drupal.toggleFieldset(this);
        });
        if ($(this).text() == "Collapse all") {
          $(this).text("Expand all");
        }
        else {
          $(this).text("Collapse all");
        }
      });
      // Collapse fieldsets that have no enabled modules.
      $("fieldset").each(function() {
        if ($(this).find("input:checkbox:checked").length === 0) {
          Drupal.toggleFieldset(this);
        }
      });

    }
  }
})(jQuery);
