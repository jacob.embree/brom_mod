<?php

/**
 * @file
 * Functions for administering Brom Mod.
 */

/**
 * Form constructor for administering Brom Mod.
 */
function brom_mod_settings_form($form, &$form_state) {
  $form['brom_mod_debug'] = array(
    "#type" => "checkbox",
    "#title" => t("Debug mode"),
    "#description" => t("Enable to allow performance log messages to appear in watchdog."),
    "#default_value" => variable_get("brom_mod_debug", FALSE),
  );
  $form['brom_mod_debug_ignore_admin'] = array(
    "#type" => "checkbox",
    "#title" => t("Ignore admin"),
    "#description" => t("Do not debug while the user has uid 1."),
    "#default_value" => variable_get("brom_mod_debug_ignore_admin", TRUE),
  );
  $form['brom_mod_debug_cookies'] = array(
    "#type" => "checkbox",
    "#title" => t("Log cookies"),
    "#description" => t("If checked then when logging the cookies passed in the request will be included."),
    "#default_value" => variable_get("brom_mod_debug_cookies", FALSE),
  );
  return system_settings_form($form);
}
